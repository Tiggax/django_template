# Django_Template



# Set up the prerequisets

Firstly you will need to setup the enviroment.

You **need**:

- Python3 (The server already has 3.8.10)
- pip 
- git ( gud )

(If you are working on your machine, you can get the required programs through your package managers)

> #### For server
>since all of the commands are being installed in your local folder, you >should run this to have them in your scope:
>
>```bash
>'export PATH="~/.local/bin:$PATH"' >> ~/.bashrc
>```


## Pip install

This is relativly easy. If you already have python.
Download the module from pip, and launch it using python:

```bash
curl curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
```

you should now have Python and pip installed.

# Enviroment setup

For easier work we will be using enviromnet virtualization.

(to learn more about [venv](https://docs.python.org/3/library/venv.html))

We will need `django` and `django-admin` to create a new project:

```bash
pip install django django-admin
```

# Set up your directory

go to a location you want to start your project in.
(For any variable that you change to your liking I will be using <>)

```bash
cd my-projects
```

## Create Django project

We will create our project:

```bash
django-admin startproject <project>
```

This creates a folder in your projects folder named \<project\>
```
<project>/
├── manage.py
└── <project>
    ├── asgi.py
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```
This will be our development folder. It has a subfolder named the same name that the project is named. ( For easier understanding of this tutorial, every time I say `root`, i mean the <project> directory, and every time i say `project` i mean the subfolder containing the settings.) In the project there is a `settings.py` file. That file contains all the settings that we need, and will be edited later to our liking.

## add Git

Next thing we do, is set up our git. This will help us keep up with changes. In the root dir run this command:
```bash
git init
```
This creates a .git folder in the root of the project.
Now we can create a `.gitignore` file, that will ignore some files, that we do not want to track. In the file add:

```.gitignore
__pycache__
venv
.env
```

## add virtual env

in the root we will run:
```bash
python -m venv .venv
```

This creates a new folder called `.venv` in the root of the project. In it a localized python runner with dependencies will reside. this will make the deployment on the server easier. To activate the enviroment you can just run the `activate` script in `<root>/venv/bin/`. This will run a special python instance created just for this project.

> **Note**
>
> The file might not be executable the first time. To fix this simply run:
>```bash
> chmod +x .venv/bin/*
>```

### add required packages:

```bash
source .venv/bin/activate
pip install django django-admin python-environ
pip freeze > requirements.txt
```

This will create a `requirements.txt` file in your root. add it to your git. 
This has a similar functionality to `package.json` in node or `cargo.toml` in rust. It saves all of your packages that your project needs to work.

You can later clone this repository to your machine, and run:
```bash
python -m venv .venv
chmod +x .venv/bin/activate
source .venv/bin/activate
pip install -r requirements.txt
```
This will install all of your dependencies that you would need.

## dotenv

Normaly you could use `python-dotenv` but i'm gonna be using `python-environ` as it has more integrated support for django.

`environ` is a tool to add secrets, and otherwise private settings and information to the project. The main file is `.env`, that contains those secrets. 

> **Note**:
>
> We do not want to add sensitive information to our git repository, thats why the `.env` file should be kept seperate, and be server specific.

We will modify our `settings.py` in our project folder.
```python
from pathlib import Path
import os                           # added
import environ                      # added
env = environ.Env(                  # added
    # set casting, default value    # added
    DEBUG=(bool, False)             # added
)                                   # added


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

environ.Env.read_env(               # added
    os.path.join(Base_DIR,'.env')   # added
)                                   # added

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('KEY')             # modified

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env('DEBUG')                # modified

ALLOWED_HOSTS = []

...
```
***SAVE*** the key in `SEKCET_KEY`, and add a line to your `.env`as:
```
KEY='<your key>'
```

When you are going to work on your project you can add `DEBUG` to your `.env` to activate debug mode. it will make debbuging easier. For the production server you should't use DEBUG.

you can also add allowed hosts and other variables like ports to your `.env`, and use them in the python scripts.
